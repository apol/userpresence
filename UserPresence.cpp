/*************************************************************************************
 *  Copyright (C) 2013 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>             *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include "UserPresence.h"
#include "konversationintegration.h"
#include "telepathyintegration.h"
#include "lockedscreenprovider.h"
#include <KPluginFactory>
#include <kdemacros.h>
#include <QDebug>

K_PLUGIN_FACTORY(UserPresenceFactory, registerPlugin<UserPresence>();)
K_EXPORT_PLUGIN(UserPresenceFactory("userpresence", "userpresence"))

UserPresence::UserPresence(QObject* parent, const QVariantList&)
    : KDEDModule(parent)
    , m_presence(true)
    , m_availability(true)
{
    new KonversationIntegration(this);
    new TelepathyIntegration(this);
    new LockedScreenProvider(this);
}

UserPresence::~UserPresence()
{}

bool UserPresence::isAvailable() const
{
    return m_availability;
}

bool UserPresence::isPresent() const
{
    return m_availability;
}

void UserPresence::setAvailable(bool availability)
{
    if(m_availability!=availability) {
        m_availability = availability;
        emit availabilityChanged(availability);
    }
}

void UserPresence::setPresent(bool presence)
{
    if(m_presence != presence) {
        m_presence = presence;
        emit presenceChanged(presence);
    }
}
