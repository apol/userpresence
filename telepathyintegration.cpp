/*************************************************************************************
 *  Copyright (C) 2013 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>             *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include "telepathyintegration.h"
#include "UserPresence.h"
#include <KTp/global-presence.h>
#include <TelepathyQt/PendingReady>

TelepathyIntegration::TelepathyIntegration(UserPresence* parent)
    : QObject(parent)
    , m_global(new KTp::GlobalPresence(this))
{
    Tp::registerTypes();
    connect(parent, SIGNAL(presenceChanged(bool)), SLOT(changeStatus()));
    connect(parent, SIGNAL(availabilityChanged(bool)), SLOT(changeStatus()));
    
    Tp::AccountFactoryPtr  accountFactory = Tp::AccountFactory::create(QDBusConnection::sessionBus(),
                                                                       Tp::Features() << Tp::Account::FeatureCore
                                                                       << Tp::Account::FeatureProtocolInfo
                                                                       << Tp::Account::FeatureProfile
                                                                       << Tp::Account::FeatureCapabilities);

    Tp::ConnectionFactoryPtr connectionFactory = Tp::ConnectionFactory::create(QDBusConnection::sessionBus(),
                                                                               Tp::Features() << Tp::Connection::FeatureCore
                                                                               << Tp::Connection::FeatureRoster
                                                                               << Tp::Connection::FeatureSelfContact);

    Tp::ChannelFactoryPtr channelFactory = Tp::ChannelFactory::create(QDBusConnection::sessionBus());

    m_accountManager = Tp::AccountManager::create(QDBusConnection::sessionBus(),
                                                  accountFactory,
                                                  connectionFactory,
                                                  channelFactory);

    connect(m_accountManager->becomeReady(), SIGNAL(finished(Tp::PendingOperation*)), SLOT(onAccountManagerReady(Tp::PendingOperation*)));
}

void TelepathyIntegration::onAccountManagerReady(Tp::PendingOperation* )
{
    m_global->setAccountManager(m_accountManager);
}

void TelepathyIntegration::changeStatus()
{
    UserPresence* presence = static_cast<UserPresence*>(parent());
    if(presence->isPresent()) {
        if(presence->isAvailable())
            m_global->setPresence(Tp::Presence::available());
        else
            m_global->setPresence(Tp::Presence::busy());
    } else {
        m_global->setPresence(Tp::Presence::away());
    }
}
