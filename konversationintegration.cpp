/*************************************************************************************
 *  Copyright (C) 2013 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>             *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include "konversationintegration.h"
#include "UserPresence.h"
#include <QDBusInterface>
#include <QDBusPendingCall>

KonversationIntegration::KonversationIntegration(UserPresence* parent)
    : QObject(parent)
{
    connect(parent, SIGNAL(presenceChanged(bool)), SLOT(changePresence(bool)));
}

void KonversationIntegration::changePresence(bool present)
{
    QDBusInterface konversation(QLatin1String("org.kde.konversation"), QLatin1String("/irc"));
    if(present) {
        konversation.asyncCall(QLatin1String("setBack"));
    } else {
        konversation.asyncCall(QLatin1String("setAway"), QString());
    }
}
