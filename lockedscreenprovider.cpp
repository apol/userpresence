/*************************************************************************************
 *  Copyright (C) 2013 by Aleix Pol Gonzalez <aleixpol@blue-systems.com>             *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include "lockedscreenprovider.h"
#include "UserPresence.h"
#include <QDBusInterface>
#include <QDBusPendingCall>
#include <QDBusPendingReply>

LockedScreenProvider::LockedScreenProvider(UserPresence* parent)
    : QObject(parent)
{
    QDBusConnection::systemBus().connect(
        QLatin1String("org.freedesktop.ScreenSaver"), QLatin1String("/ScreenSaver"),
        QLatin1String("org.freedesktop.ScreenSaver"), QLatin1String("ActiveChanged"),
        this, SLOT(activeChanged()));
}

void LockedScreenProvider::activeChanged()
{
    QDBusInterface interface(QLatin1String("org.freedesktop.ScreenSaver"), QLatin1String("/ScreenSaver"),
                             QLatin1String("org.freedesktop.ScreenSaver"));
    QDBusPendingCall call = interface.asyncCall(QLatin1String("GetActive"));
    QDBusPendingCallWatcher* watcher = new QDBusPendingCallWatcher(call, this);
    connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)), SLOT(screenshotActivityChanged(QDBusPendingCallWatcher*)));
}

void LockedScreenProvider::screenshotActivityChanged(QDBusPendingCallWatcher* watcher)
{
    QDBusPendingReply<bool> callScreensaverIsOn(*watcher);
    UserPresence* presence = static_cast<UserPresence*>(parent());
    presence->setPresent(!callScreensaverIsOn.value());
}
